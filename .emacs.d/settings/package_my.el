;; Package manager:
;; Initialise package and add Melpa repository

(require 'package)

(setq my-packages
    '(
      el-get
      company-mode
      company-flx
      expand-region
      flycheck
      helm
      helm-projectile
      helm-swoop
      key-chord
      magit
      markdown-mode
      multiple-cursors
      neotree
      smartparens
      smart-mode-line
      )
)

(when (executable-find "python")

    (add-to-list 'my-packages 'pip-requirements)
    (when (executable-find "autopep8")
      (add-to-list 'my-packages 'py-autopep8)
      )
    (add-to-list 'my-packages 'py-isort)
	(when (executable-find "virtualenv")
      		(add-to-list 'my-packages 'auto-virtualenv))

)

;; for gnu repository
(setq package-check-signature nil)

(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/") t)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/") t)
;;(package-initialize)
(add-to-list 'load-path "~/.emacs.d/el-get/el-get")
;;(package-initialize)

(unless (require 'el-get nil t)
  (package-refresh-contents)
;;  (package-install 'el-get)
;;  (package-install 'async)
;;  (package-install 'jsonrpc)
;;  (package-install 'memoize)
;;  (message "require is")
  (require 'el-get)
  (el-get 'sync))

(add-to-list 'el-get-recipe-path "~/.emacs.d/settings/recipes")
(el-get 'sync my-packages)

(provide 'package_my)
